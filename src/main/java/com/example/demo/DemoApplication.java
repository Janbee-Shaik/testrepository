package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}

@RestController
 class HelloController {
	 

	@GetMapping("/hello")
    public String hello(){
    return "Hello world";
}

	@GetMapping("/sayhello")
    public String sayHello(){
    return "Hello team";
}


	@GetMapping("/wish")
    public String wish()
    return "hello team ,Congratualations!";
}


}